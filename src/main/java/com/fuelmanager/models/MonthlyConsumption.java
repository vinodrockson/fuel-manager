package com.fuelmanager.models;

import java.util.List;

public class MonthlyConsumption {
	
	String month;
	int year;
	List<FuelConsumption> fuelConsumption;
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public List<FuelConsumption> getFuelConsumption() {
		return fuelConsumption;
	}
	public void setFuelConsumption(List<FuelConsumption> fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

}
