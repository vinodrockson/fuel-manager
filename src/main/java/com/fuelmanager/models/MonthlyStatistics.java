package com.fuelmanager.models;

import java.util.List;

public class MonthlyStatistics {
	String month;
	int year;
	List<FuelStat> fuelStat;
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public List<FuelStat> getFuelStat() {
		return fuelStat;
	}
	public void setFuelStat(List<FuelStat> fuelStat) {
		this.fuelStat = fuelStat;
	}
}
