package com.fuelmanager.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.fuelmanager.models.FuelRegister;


@Repository
public interface FuelRegisterRepository extends JpaRepository<FuelRegister, Long> {
	
	@Query("SELECT SUM(price * volume), MONTH(regDate), YEAR(regDate) FROM FuelRegister GROUP BY MONTH(regDate),YEAR(regDate)")
	List<Object[]> getAllMonthlyTotals();	
	
	@Query("SELECT fuelType, volume, regDate, price, (price * volume), driverId FROM FuelRegister f WHERE MONTH(regDate) = :iMonth and YEAR(regDate) = :iYear")
	List<Object[]> getAllMonthlyConsumption(@Param("iMonth") int iMonth, @Param("iYear") int iYear);	
	
	@Query("SELECT fuelType, SUM(volume), AVG(price), SUM(price * volume), MONTH(regDate), YEAR(regDate) FROM FuelRegister GROUP BY fuelType, MONTH(regDate),YEAR(regDate) ORDER BY YEAR(regDate), MONTH(regDate)")
	List<Object[]> getAllMonthlyStat();	
	
	@Query("SELECT SUM(price * volume), MONTH(regDate), YEAR(regDate) FROM FuelRegister WHERE driverId = :driverid GROUP BY MONTH(regDate),YEAR(regDate)")
	List<Object[]> getMonthlyTotalsByDriverId(@Param("driverid") Long driverid);	
	
	@Query("SELECT fuelType, volume, regDate, price, (price * volume), driverId FROM FuelRegister f WHERE driverId = :driverid and MONTH(regDate) = :iMonth and YEAR(regDate) = :iYear")
	List<Object[]> getMonthlyConsumptionByDriverId(@Param("iMonth") int iMonth, @Param("iYear") int iYear, @Param("driverid") Long driverid);	
	
	@Query("SELECT fuelType, SUM(volume), AVG(price), SUM(price * volume), MONTH(regDate), YEAR(regDate) FROM FuelRegister WHERE driverId = :driverid GROUP BY fuelType, MONTH(regDate),YEAR(regDate) ORDER BY YEAR(regDate), MONTH(regDate)")
	List<Object[]> getMonthlyStatByDriverId(@Param("driverid") Long driverid);	
	
	@Query("SELECT f FROM FuelRegister f WHERE f.driverId = :driverid")
	List<FuelRegister> findDriver(@Param("driverid") Long driverid);
}
