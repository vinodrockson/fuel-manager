package com.fuelmanager.assemblers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import com.fuelmanager.controllers.FuelRegisterController;
import com.fuelmanager.models.FuelRegister;
import com.fuelmanager.resources.FuelRegisterResource;


public class FuelRegisterAssembler extends ResourceAssemblerSupport<FuelRegister, FuelRegisterResource> {
	
	public FuelRegisterAssembler() {
		super(FuelRegisterController.class, FuelRegisterResource.class);
	}

	@Override
	public FuelRegisterResource toResource(FuelRegister fuelRegister) {

		FuelRegisterResource fuelResource = createResourceWithId(fuelRegister.getId(), fuelRegister);
		fuelResource.setDriverId(fuelRegister.getDriverId());
		fuelResource.setFuelType(fuelRegister.getFuelType());
		fuelResource.setPrice(fuelRegister.getPrice());
		fuelResource.setRegDate(fuelRegister.getRegDate());
		fuelResource.setVolume(fuelRegister.getVolume());
			
		return fuelResource;
	}

	public List<FuelRegisterResource> toResource(List<FuelRegister> fuels) {
		List<FuelRegisterResource> fuelList = new ArrayList<>();

		for (FuelRegister fuel : fuels) {
			fuelList.add(toResource(fuel));
		}
		return fuelList;
	}

}
