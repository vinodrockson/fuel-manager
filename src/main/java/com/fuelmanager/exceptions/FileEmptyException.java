package com.fuelmanager.exceptions;

public class FileEmptyException extends Exception{
	private static final long serialVersionUID = 1L;

	public FileEmptyException() {
		super("File is empty. Input correct XML file");
	}

}
