package com.fuelmanager.exceptions;

public class ResourceValidationException extends Exception{

	private static final long serialVersionUID = 1L;

	public ResourceValidationException(String message) {
		super(message);
	}
}
