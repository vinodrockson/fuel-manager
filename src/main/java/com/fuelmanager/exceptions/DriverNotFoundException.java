package com.fuelmanager.exceptions;

public class DriverNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	public DriverNotFoundException(Long id) {
		super(String.format("Driver not found! (Driver id: %d)", id));
	}

}
