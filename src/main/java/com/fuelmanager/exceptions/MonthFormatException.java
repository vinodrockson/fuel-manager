package com.fuelmanager.exceptions;

public class MonthFormatException extends Exception {
	private static final long serialVersionUID = 1L;

	public MonthFormatException(String month) {
		super("The given month: " + month + " is in wrong format or values. Use MM.yyyy with correct values");
	}
}

