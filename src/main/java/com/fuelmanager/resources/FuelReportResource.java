package com.fuelmanager.resources;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fuelmanager.models.MonthlyConsumption;
import com.fuelmanager.models.MonthlySpendings;
import com.fuelmanager.models.MonthlyStatistics;
import com.fuelmanager.support.ResourceSupport;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FuelReportResource extends ResourceSupport{
	
	List<MonthlySpendings> monthlySpendings;
	MonthlyConsumption monthlyConsumption;
	List<MonthlyStatistics> monthlyStatistics;
	
	public List<MonthlySpendings> getMonthlySpendings() {
		return monthlySpendings;
	}
	public void setMonthlySpendings(List<MonthlySpendings> monthlySpendings) {
		this.monthlySpendings = monthlySpendings;
	}
	public MonthlyConsumption getMonthlyConsumption() {
		return monthlyConsumption;
	}
	public void setMonthlyConsumption(MonthlyConsumption monthlyConsumption) {
		this.monthlyConsumption = monthlyConsumption;
	}
	public List<MonthlyStatistics> getMonthlyStatistics() {
		return monthlyStatistics;
	}
	public void setMonthlyStatistics(List<MonthlyStatistics> monthlyStatistics) {
		this.monthlyStatistics = monthlyStatistics;
	}
	
}
