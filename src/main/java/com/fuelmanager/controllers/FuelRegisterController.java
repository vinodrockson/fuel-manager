package com.fuelmanager.controllers;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fuelmanager.assemblers.FuelRegisterAssembler;
import com.fuelmanager.exceptions.DriverNotFoundException;
import com.fuelmanager.exceptions.FileEmptyException;
import com.fuelmanager.exceptions.MonthFormatException;
import com.fuelmanager.exceptions.ResourceValidationException;
import com.fuelmanager.models.ErrorResponse;
import com.fuelmanager.models.FuelConsumption;
import com.fuelmanager.models.FuelRegister;
import com.fuelmanager.models.FuelStat;
import com.fuelmanager.models.MonthlyConsumption;
import com.fuelmanager.models.MonthlySpendings;
import com.fuelmanager.models.MonthlyStatistics;
import com.fuelmanager.repositories.FuelRegisterRepository;
import com.fuelmanager.resources.FuelRegisterResource;
import com.fuelmanager.resources.FuelReportResource;

@RestController
@RequestMapping(value="/rest/fuel")
public class FuelRegisterController {
	
	@Autowired
	FuelRegisterRepository frepo;
	
	FuelRegisterAssembler fuelAssembler = new FuelRegisterAssembler();
	
	// Register Fuel single record
    @RequestMapping(value="/registerone", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<FuelRegisterResource> registerSingleFuel(@RequestBody FuelRegisterResource fuelResource) throws URISyntaxException, ResourceValidationException{
    	
    	String errMessage = validateResource(fuelResource);
    	if (!errMessage.isEmpty()) {
    		throw new ResourceValidationException(errMessage);
		}

    	FuelRegister fuel = new FuelRegister();
    	fuel.setDriverId(fuelResource.getDriverId());
    	fuel.setFuelType(fuelResource.getFuelType());
    	fuel.setPrice(fuelResource.getPrice());
    	fuel.setRegDate(fuelResource.getRegDate());
    	fuel.setVolume(fuelResource.getVolume());    	
    	
		FuelRegister createdFuel = frepo.save(fuel);
		FuelRegisterResource fRes = fuelAssembler.toResource(createdFuel);
		HttpHeaders headers = new HttpHeaders();
	    headers.setLocation(new URI(fRes.getId().getHref()));	   		    	
    	return new ResponseEntity<FuelRegisterResource>(fRes, headers, HttpStatus.CREATED);
    }
    
    // Register Fuel Bulk record as file
    @RequestMapping(value="/registerbulk", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<FuelRegisterResource> registerBulkFuel(@RequestParam("file") MultipartFile file) throws DOMException, ParseException, IOException, ParserConfigurationException, SAXException, FileEmptyException, ResourceValidationException{

    	if(file.isEmpty()) {
    		throw new FileEmptyException();
    	}
    	
    	InputStream is = file.getInputStream();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();                 
        Document doc = dBuilder.parse(is);
        doc.getDocumentElement().normalize();
		NodeList nList = doc.getElementsByTagName("fuel");
		List<FuelRegister> fuelList = new ArrayList<>();
		
		for (int temp = 0; temp < nList.getLength(); temp++) {  
			Node nNode = nList.item(temp);			

			if (nNode.getNodeType() == Node.ELEMENT_NODE){						
				Element eElement = (Element) nNode;	 
				FuelRegister fuel = new FuelRegister();
				fuel.setDriverId(Long.parseLong(eElement.getElementsByTagName("driverid").item(0).getTextContent()));
				fuel.setFuelType(eElement.getElementsByTagName("fueltype").item(0).getTextContent());
				fuel.setPrice(new BigDecimal(eElement.getElementsByTagName("price").item(0).getTextContent()).setScale(2, RoundingMode.HALF_EVEN));
				fuel.setVolume(new BigDecimal(eElement.getElementsByTagName("volume").item(0).getTextContent()));
				Date regDate = new SimpleDateFormat("MM.dd.yyyy").parse(eElement.getElementsByTagName("regdate").item(0).getTextContent()); 
				fuel.setRegDate(regDate);
				
		    	String errMessage = validateXMLdata(fuel);
		    	if (!errMessage.isEmpty()) {
		    		fuelList = null;
		    		throw new ResourceValidationException(errMessage);
				}
		    	else
		    		fuelList.add(fuel);		    	
				
			}
		}			
    	 
		for(FuelRegister fuel : fuelList) {
			frepo.save(fuel);
		}
		
    	return new ResponseEntity<FuelRegisterResource>(HttpStatus.CREATED);
    } 
    
    // Retrieve information about fuel records
	@RequestMapping(method = RequestMethod.GET, value = "report")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<FuelReportResource> getReport(@RequestParam("month") String month, @RequestParam(value="driverid",required=false) Long driverid) throws ParseException, MonthFormatException, DriverNotFoundException {
		
		if(!month.matches("^\\s*(1[012]|0?[1-9])\\.((?:19|20)\\d{2})\\s*$")) {
			throw new MonthFormatException(month);
		}
		
		if(driverid != null) {
			List<FuelRegister> fuelRegisterList  = frepo.findDriver(driverid);
			if(fuelRegisterList.size() == 0) {
				throw new DriverNotFoundException(driverid);
			}
		}
		
		// 1
		List<Object[]> totalFuels = new ArrayList<>(); 
		if(driverid != null) {
			totalFuels = frepo.getMonthlyTotalsByDriverId(driverid); 
		}
		else {
			totalFuels = frepo.getAllMonthlyTotals(); 
		}	
    	List<MonthlySpendings> msList = new ArrayList<>(); 
		for (Object[] fuel: totalFuels) {
	 		MonthlySpendings ms = new MonthlySpendings();
	 		ms.setTotalSpent(new BigDecimal(fuel[0].toString()).setScale(2, RoundingMode.HALF_EVEN));
	 		int monthNumber= Integer.parseInt(fuel[1].toString());
	 		ms.setMonth(Month.of(monthNumber).name());
	 		ms.setYear(Integer.parseInt(fuel[2].toString()));	 		
	 		msList.add(ms);
		}
					
		// 2
		int iMonth = Integer.parseInt(month.split("\\.")[0]);
		int iYear = Integer.parseInt(month.split("\\.")[1]);
		List<Object[]> monthlyFuel = new ArrayList<>(); 
		if(driverid != null) {
			monthlyFuel = frepo.getMonthlyConsumptionByDriverId(iMonth, iYear, driverid); 
		}
		else {
			monthlyFuel = frepo.getAllMonthlyConsumption(iMonth, iYear); 
		}			
		List<FuelConsumption> fcList = new ArrayList<>(); 
		for (Object[] fuel: monthlyFuel) {
	 		FuelConsumption fc = new FuelConsumption();
	 		fc.setFuelType(fuel[0].toString());
	 		fc.setVolume(new BigDecimal(fuel[1].toString()));
	 		Date regDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(fuel[2].toString()); 
	 		fc.setRegDate(regDate);
	 		fc.setPrice(new BigDecimal(fuel[3].toString()).setScale(2, RoundingMode.HALF_EVEN));
	 		fc.setTotalPrice(new BigDecimal(fuel[4].toString()).setScale(2, RoundingMode.HALF_EVEN));
	 		fc.setDriverId(Long.parseLong(fuel[5].toString()));
	 		fcList.add(fc);
		}		
		MonthlyConsumption mc = new MonthlyConsumption();
		mc.setMonth(Month.of(iMonth).name());
		mc.setYear(iYear);
		mc.setFuelConsumption(fcList);
		
	
		// 3
		List<Object[]> monthlyStat = new ArrayList<>(); 
		if(driverid != null) {
			monthlyStat = frepo.getMonthlyStatByDriverId(driverid); 
		}
		else {
			monthlyStat = frepo.getAllMonthlyStat(); 
		}	
		List<MonthlyStatistics> statList = new ArrayList<>(); 
		int i=0;
		while ( i<monthlyStat.size()) {
			Object[] fuel = monthlyStat.get(i);
			
			List<FuelStat> fsList = new ArrayList<>();
			MonthlyStatistics monthlyStatistics = new MonthlyStatistics();
			int fMonth = Integer.parseInt(fuel[4].toString());
			monthlyStatistics.setMonth(Month.of(fMonth).name());
			int fYear = Integer.parseInt(fuel[5].toString());
			monthlyStatistics.setYear(fYear);

			List<Object[]> sortedFuel = new ArrayList<>();
			do {
				sortedFuel.add(monthlyStat.get(i));
				i++;
			}while(i<monthlyStat.size() && Integer.parseInt(monthlyStat.get(i)[4].toString()) == fMonth && Integer.parseInt(monthlyStat.get(i)[5].toString()) == fYear);
			
			for(Object[] sFuel : sortedFuel) {			
		 		FuelStat fs = new FuelStat();
		 		fs.setFuelType(sFuel[0].toString());
		 		fs.setTotalVolume(new BigDecimal(sFuel[1].toString()).setScale(2, RoundingMode.HALF_EVEN));
		 		fs.setAveragePrice(new BigDecimal(sFuel[2].toString()).setScale(2, RoundingMode.HALF_EVEN));
		 		fs.setTotalPrice(new BigDecimal(sFuel[3].toString()).setScale(2, RoundingMode.HALF_EVEN));
		 		fsList.add(fs);	 		
			}
				
	 		monthlyStatistics.setFuelStat(fsList);
	 		statList.add(monthlyStatistics);
		}				
		
		FuelReportResource fuelReportRes = new FuelReportResource();
		fuelReportRes.setMonthlySpendings(msList);
		fuelReportRes.setMonthlyConsumption(mc);
		fuelReportRes.setMonthlyStatistics(statList);
    	return new ResponseEntity<FuelReportResource>(fuelReportRes, HttpStatus.OK);
    }
	
	private String validateResource(FuelRegisterResource fResource) {
		String fuelType = fResource.getFuelType();
		BigDecimal price = fResource.getPrice(), volume = fResource.getVolume();
		Date regDate = fResource.getRegDate();
		Long driverId = fResource.getDriverId();
		String response = "";
		List<String> fuelTypes = Arrays.asList("95","98","D");
		

		if (fuelType.isBlank() || !fuelTypes.contains(fuelType))  {
			response = "Missing fuelType or wrong values. Possible values are 95,98 or D";
			return response;
		}
		
		if (price == null || price.signum() <= 0.00 )  {
			response = "Missing price or wrong values. Value should be greater than 0.00";
			return response;
		}
		
		if (volume == null || volume.signum() <= 0.00 )  {
			response = "Missing volume or wrong values. Value should be greater than 0.00";
			return response;
		}
		
		if (regDate == null || !new SimpleDateFormat("MM.dd.yyyy").format(regDate).matches("^\\s*(1[012]|0?[1-9])\\.(3[01]|[12][0-9]|0?[1-9])\\.((?:19|20)\\d{2})\\s*$"))  {
			response = "Missing regDate or wrong values. Date format is MM.dd.yyyy";
			return response;
		}		
		
		if (driverId == null || driverId <= 0 )  {
			response = "Missing driverId or wrong values. Value should be greater than 0";
			return response;
		}
		
		return response;
	}
	
	private String validateXMLdata(FuelRegister fuelRegister) {
		String fuelType = fuelRegister.getFuelType();
		BigDecimal price = fuelRegister.getPrice(), volume = fuelRegister.getVolume();
		Date regDate = fuelRegister.getRegDate();
		Long driverId = fuelRegister.getDriverId();
		String response = "";
		String responseStr = " File should be completely error-free to save all the data.";
		List<String> fuelTypes = Arrays.asList("95","98","D");
		
		if (fuelType.isBlank() || !fuelTypes.contains(fuelType))  {
			response = "In one of the element, missing fueltype or wrong values. Possible values are 95,98 or D." + responseStr;
			return response;
		}
		
		if (price == null || price.signum() <= 0.00 )  {
			response = "In one of the element, missing price or wrong values. Value should be greater than 0.00." + responseStr;
			return response;
		}
		
		if (volume == null || volume.signum() <= 0.00 )  {
			response = "In one of the element, missing volume or wrong values. Value should be greater than 0.00." + responseStr;
			return response;
		}
		
		if (regDate == null || !new SimpleDateFormat("MM.dd.yyyy").format(regDate).matches("^\\s*(1[012]|0?[1-9])\\.(3[01]|[12][0-9]|0?[1-9])\\.((?:19|20)\\d{2})\\s*$"))  {
			response = "In one of the element, missing regdate or wrong values. Date format is MM.dd.yyyy." + responseStr;
			return response;
		}		
		
		if (driverId == null || driverId <= 0 )  {
			response = "In one of the element, Missing driverid or wrong values. Value should be greater than 0." + responseStr;
			return response;
		}
				
		return response;
	}
	
	@ExceptionHandler(MonthFormatException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<ErrorResponse> handleMonthFormatException(MonthFormatException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.BAD_REQUEST.value());
		errorResponse.setErrorDetails(HttpStatus.BAD_REQUEST.getReasonPhrase());
		errorResponse.setErrorMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(DriverNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<ErrorResponse> handleDriverNotFoundException(DriverNotFoundException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.NOT_FOUND.value());
		errorResponse.setErrorDetails(HttpStatus.NOT_FOUND.getReasonPhrase());
		errorResponse.setErrorMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.NOT_FOUND);
	}
	
	
	@ExceptionHandler(ResourceValidationException.class)
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	public ResponseEntity<ErrorResponse> handleResourceValidationException(ResourceValidationException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.UNPROCESSABLE_ENTITY.value());
		errorResponse.setErrorDetails(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase());
		errorResponse.setErrorMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.UNPROCESSABLE_ENTITY);
	}
	
	@ExceptionHandler(FileEmptyException.class)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<ErrorResponse> handleFileEmptyException(FileEmptyException ex) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(HttpStatus.NO_CONTENT.value());
		errorResponse.setErrorDetails(HttpStatus.NO_CONTENT.getReasonPhrase());
		errorResponse.setErrorMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.NO_CONTENT);
	}
	
}
