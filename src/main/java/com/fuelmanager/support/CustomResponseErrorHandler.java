package com.fuelmanager.support;

import java.io.IOException;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;

public class CustomResponseErrorHandler implements ResponseErrorHandler {
	
	private ResponseErrorHandler errorHandler = new DefaultResponseErrorHandler();

    @Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
        return errorHandler.hasError(response);
    }

    @Override
	public void handleError(ClientHttpResponse response) throws IOException {    
    }
}
