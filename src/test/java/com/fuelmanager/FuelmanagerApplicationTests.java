package com.fuelmanager;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fuelmanager.exceptions.DriverNotFoundException;
import com.fuelmanager.exceptions.MonthFormatException;
import com.fuelmanager.exceptions.ResourceValidationException;
import com.fuelmanager.models.ErrorResponse;
import com.fuelmanager.repositories.FuelRegisterRepository;
import com.fuelmanager.resources.FuelRegisterResource;
import com.fuelmanager.resources.FuelReportResource;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { FuelmanagerApplication.class })
@ActiveProfiles("test")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class FuelmanagerApplicationTests {
	
	@Autowired
	FuelRegisterRepository fRepo;

	@Autowired
	private WebApplicationContext wac;
	
	private MockMvc mockMvc;
	private ObjectMapper mapper = new ObjectMapper();
	
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}
	
	@Test
	@DatabaseSetup("dataset.xml")
	public void contextLoads() {
		assertEquals(292, fRepo.count());	
	}

	@Test
	@DatabaseSetup("dataset.xml")
	public void checkDriver() {
		assertEquals(1, fRepo.findDriver(249701L).size());	
	}
	
	@Test
	@DatabaseSetup(value = "EmptyDatabase.xml")
	public void testRegisterSingle() throws Exception {
	FuelRegisterResource fuelRegisterResource = new FuelRegisterResource();
	fuelRegisterResource.setIdRes(1L);
	fuelRegisterResource.setDriverId(12569L);
	fuelRegisterResource.setFuelType("95");
	fuelRegisterResource.setPrice(new BigDecimal(78.90));
	fuelRegisterResource.setVolume(new BigDecimal(10));
	fuelRegisterResource.setRegDate(new SimpleDateFormat("MM.dd.yyyy").parse("11.23.2019"));

		MvcResult result = mockMvc
				.perform(
						post("/rest/fuel/registerone").contentType(
								MediaType.APPLICATION_JSON).content(
								mapper.writeValueAsString(fuelRegisterResource)))
				.andExpect(status().isCreated()).andReturn();

		FuelRegisterResource fRes = mapper.readValue(result.getResponse()
				.getContentAsString(), FuelRegisterResource.class);
		
		assertEquals(fRes.getFuelType(),"95");
	}
	
	//Fueltype validation
	@Test
	@DatabaseSetup(value = "EmptyDatabase.xml")
	public void testRegisterSingleForDataValidation() throws Exception {
	FuelRegisterResource fuelRegisterResource = new FuelRegisterResource();
	fuelRegisterResource.setIdRes(1L);
	fuelRegisterResource.setDriverId(12569L);
	fuelRegisterResource.setFuelType("87");
	fuelRegisterResource.setPrice(new BigDecimal(78.90));
	fuelRegisterResource.setVolume(new BigDecimal(10));
	fuelRegisterResource.setRegDate(new SimpleDateFormat("MM.dd.yyyy").parse("11.23.2019"));

		MvcResult result = mockMvc
				.perform(
						post("/rest/fuel/registerone").contentType(
								MediaType.APPLICATION_JSON).content(
								mapper.writeValueAsString(fuelRegisterResource)))
				.andExpect(status().isUnprocessableEntity()).andReturn();

		ErrorResponse error = mapper.readValue(result.getResponse()
				.getContentAsString(), ErrorResponse.class);
		
		assertThatExceptionOfType(ResourceValidationException.class).isThrownBy(() -> { throw new ResourceValidationException("Missing fuelType or wrong values. Possible values are 95,98 or D"); })
        .withMessage("Missing fuelType or wrong values. Possible values are 95,98 or D"); 
		
		assertEquals(error.getErrorCode(), HttpStatus.UNPROCESSABLE_ENTITY.value());
		assertEquals(error.getErrorMessage(), "Missing fuelType or wrong values. Possible values are 95,98 or D");
	}
	
    @Test
    public void testRegisterBulkFileUpload() throws Exception {

        String fileName = "test.xml";
        String data = "<?xml version=\"1.0\"?>\n" + 
        		"<register>\n" + 
        		"	<fuel>\n" + 
        		"		<driverid>124392</driverid>\n" + 
        		"		<fueltype>D</fueltype>\n" + 
        		"		<price>7.89</price>\n" + 
        		"		<volume>2.45</volume>\n" + 
        		"		<regdate>08.11.2017</regdate>\n" + 
        		"	</fuel>\n" + 
        		"	<fuel>\n" + 
        		"		<driverid>124392</driverid>\n" + 
        		"		<fueltype>D</fueltype>\n" + 
        		"		<price>9.895</price>\n" + 
        		"		<volume>2.4545</volume>\n" + 
        		"		<regdate>08.11.2017</regdate>\n" + 
        		"	</fuel>\n" + 
        		"     <fuel>\n" + 
        		"		<driverid>124392</driverid>\n" + 
        		"		<fueltype>D</fueltype>\n" + 
        		"		<price>9.89</price>\n" + 
        		"		<volume>2.45</volume>\n" + 
        		"		<regdate>08.11.2019</regdate>\n" + 
        		"	</fuel>\n" + 
        		"	<fuel>\n" + 
        		"		<driverid>127862</driverid>\n" + 
        		"		<fueltype>95</fueltype>\n" + 
        		"		<price>2.545453</price>\n" + 
        		"		<volume>2</volume>\n" + 
        		"		<regdate>01.09.2019</regdate>\n" + 
        		"	</fuel>\n" + 
        		"</register>";
        
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file",fileName,
                "application/xml", data.getBytes());

        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.multipart("/rest/fuel/registerbulk")
                                      .file(mockMultipartFile);
        
        this.mockMvc.perform(builder).andExpect(status().isCreated())
                    .andDo(MockMvcResultHandlers.print());
    }
    
    @Test
    public void testRegisterBulkFileException() throws Exception {
        String fileName = "test.xml";
        String data = "";
        
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file",fileName,
                "application/xml", data.getBytes());

        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.multipart("/rest/fuel/registerbulk")
                                      .file(mockMultipartFile);
        
        this.mockMvc.perform(builder).andExpect(status().isNoContent())
                    .andDo(MockMvcResultHandlers.print());
    }
    
    //Price Validation
    @Test
    public void testRegisterBulkFileResourceException() throws Exception {
        String fileName = "test.xml";
        String data = "<?xml version=\"1.0\"?>\n" + 
        		"<register>\n" + 
        		"	<fuel>\n" + 
        		"		<driverid>124392</driverid>\n" + 
        		"		<fueltype>D</fueltype>\n" + 
        		"		<price>7.89</price>\n" + 
        		"		<volume>2.45</volume>\n" + 
        		"		<regdate>08.11.2017</regdate>\n" + 
        		"	</fuel>\n" + 
        		"	<fuel>\n" + 
        		"		<driverid>124392</driverid>\n" + 
        		"		<fueltype>D</fueltype>\n" + 
        		"		<price>9.895</price>\n" + 
        		"		<volume>2.4545</volume>\n" + 
        		"		<regdate>08.11.2017</regdate>\n" + 
        		"	</fuel>\n" + 
        		"     <fuel>\n" + 
        		"		<driverid>124392</driverid>\n" + 
        		"		<fueltype>D</fueltype>\n" + 
        		"		<price>0.00</price>\n" + 
        		"		<volume>2.45</volume>\n" + 
        		"		<regdate>08.11.2019</regdate>\n" + 
        		"	</fuel>\n" + 
        		"	<fuel>\n" + 
        		"		<driverid>127862</driverid>\n" + 
        		"		<fueltype>95</fueltype>\n" + 
        		"		<price>2.545453</price>\n" + 
        		"		<volume>2</volume>\n" + 
        		"		<regdate>01.09.2019</regdate>\n" + 
        		"	</fuel>\n" + 
        		"</register>";
        
        MockMultipartFile mockMultipartFile = new MockMultipartFile("file",fileName,
                "application/xml", data.getBytes());

        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.multipart("/rest/fuel/registerbulk")
                                      .file(mockMultipartFile);
        
        this.mockMvc.perform(builder).andExpect(status().isUnprocessableEntity())
                    .andDo(MockMvcResultHandlers.print());
        
		assertThatExceptionOfType(ResourceValidationException.class).isThrownBy(() -> { throw new ResourceValidationException("In one of the element, missing price or wrong values. Value should be greater than 0.00. File should be completely error-free to save all the data."); })
        .withMessage("In one of the element, missing price or wrong values. Value should be greater than 0.00. File should be completely error-free to save all the data."); 
        
    }
    
	@Test
	@DatabaseSetup("dataset.xml")
	public void testGetReport() throws Exception {

		MvcResult result = mockMvc
				.perform(get("/rest/fuel/report?month=02.2019")).andExpect(status().isOk()).andReturn();

		FuelReportResource reportResource = mapper.readValue(result.getResponse()
				.getContentAsString(), FuelReportResource.class);
		
		assertEquals(reportResource.getMonthlySpendings().size(), 18);
		assertEquals(reportResource.getMonthlyConsumption().getFuelConsumption().size(), 12);
		assertEquals(reportResource.getMonthlyConsumption().getMonth(),"FEBRUARY");
		assertEquals(reportResource.getMonthlyConsumption().getYear(),2019);
		assertEquals(reportResource.getMonthlyStatistics().get(13).getFuelStat().get(0).getTotalPrice(), new BigDecimal(63410.21).setScale(2, RoundingMode.HALF_EVEN));
	}
	
	@Test
	@DatabaseSetup("dataset.xml")
	public void testGetReportMonthFormatException() throws Exception {

		MvcResult result = mockMvc
				.perform(get("/rest/fuel/report?month=13/2019")).andExpect(status().isBadRequest()).andReturn();

		ErrorResponse error = mapper.readValue(result.getResponse()
				.getContentAsString(), ErrorResponse.class);
		
		assertThatExceptionOfType(MonthFormatException.class);
		
		assertEquals(error.getErrorCode(), HttpStatus.BAD_REQUEST.value());
		assertEquals(error.getErrorMessage(), "The given month: 13/2019 is in wrong format or values. Use MM.yyyy with correct values");
	}
	
	@Test
	@DatabaseSetup("dataset.xml")
	public void testGetReportDriverNotFoundException() throws Exception {

		MvcResult result = mockMvc
				.perform(get("/rest/fuel/report?month=01.2019&driverid=567")).andExpect(status().isNotFound()).andReturn();

		ErrorResponse error = mapper.readValue(result.getResponse()
				.getContentAsString(), ErrorResponse.class);
		
		assertThatExceptionOfType(DriverNotFoundException.class);
		
		assertEquals(error.getErrorCode(), HttpStatus.NOT_FOUND.value());
		assertEquals(error.getErrorMessage(), "Driver not found! (Driver id: 567)");
	}
}
